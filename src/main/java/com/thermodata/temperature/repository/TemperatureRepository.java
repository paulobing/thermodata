package com.thermodata.temperature.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.thermodata.temperature.model.TemperatureReading;

@Repository
public interface TemperatureRepository extends CrudRepository<TemperatureReading, Long> {
    List<TemperatureReading> findAllByZoneIdOrderByReadingTimeDesc(Long zoneId);
}
