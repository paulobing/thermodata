package com.thermodata.temperature.service;

import java.util.List;

import com.thermodata.temperature.dto.TemperatureReadingDTO;

public interface TemperatureService {
    TemperatureReadingDTO saveTemperature(TemperatureReadingDTO temperatureReadingDTO);
    List<TemperatureReadingDTO> listTemperaturesByZoneId(Long zoneId);
}
