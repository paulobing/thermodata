package com.thermodata.temperature.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thermodata.temperature.dto.TemperatureReadingDTO;
import com.thermodata.temperature.model.TemperatureReading;
import com.thermodata.temperature.repository.TemperatureRepository;

@Service
public class TemperatureServiceImpl implements TemperatureService {
    private TemperatureRepository temperatureRepository;
    private ModelMapper modelMapper;

    @Autowired
    public TemperatureServiceImpl(TemperatureRepository temperatureRepository, ModelMapper modelMapper) {
        this.temperatureRepository = temperatureRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public TemperatureReadingDTO saveTemperature(TemperatureReadingDTO temperatureReadingDTO) {
        TemperatureReading temperatureReading = temperatureRepository.save(convert(temperatureReadingDTO));
        return convert(temperatureReading);
    }

    @Override
    public List<TemperatureReadingDTO> listTemperaturesByZoneId(Long zoneId) {
        return convert(temperatureRepository.findAllByZoneIdOrderByReadingTimeDesc(zoneId));
    }

    private List<TemperatureReadingDTO> convert(List<TemperatureReading> listTemperatureReading) {
        java.lang.reflect.Type targetListType = new TypeToken<List<TemperatureReadingDTO>>() {}.getType();
        return modelMapper.map(listTemperatureReading, targetListType);
    }

    private TemperatureReading convert(TemperatureReadingDTO temperatureReadingDTO) {
        return modelMapper.typeMap(TemperatureReadingDTO.class, TemperatureReading.class)
                //.addMappings(mapper -> mapper.skip(TemperatureReading::setReadingTime))
                .map(temperatureReadingDTO);
    }
    
    private TemperatureReadingDTO convert(TemperatureReading temperatureReading) {
        return modelMapper.typeMap(TemperatureReading.class, TemperatureReadingDTO.class).map(temperatureReading);
    }

}
