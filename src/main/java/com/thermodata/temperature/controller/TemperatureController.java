package com.thermodata.temperature.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.thermodata.temperature.dto.TemperatureReadingDTO;
import com.thermodata.temperature.service.TemperatureService;

@RestController
@RequestMapping("api/v1/temperature")
public class TemperatureController {
    private TemperatureService temperatureService;

    @Autowired
    public TemperatureController(TemperatureService temperatureService) {
        this.temperatureService = temperatureService;
    }
    
    @PostMapping
    public ResponseEntity<TemperatureReadingDTO> saveTemperature(@RequestBody TemperatureReadingDTO temperatureReadingDTO) {
        return new ResponseEntity<>(temperatureService.saveTemperature(temperatureReadingDTO), HttpStatus.OK);
    }

    @GetMapping("/{zoneId}")
    public ResponseEntity<List<TemperatureReadingDTO>> listTemperaturesByZoneId(@PathVariable Long zoneId) {
        return new ResponseEntity<>(temperatureService.listTemperaturesByZoneId(zoneId), HttpStatus.OK);
    }

}
