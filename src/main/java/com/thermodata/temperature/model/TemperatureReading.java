package com.thermodata.temperature.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters.LocalDateTimeConverter;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name="temperature_reading")
@NoArgsConstructor(access=AccessLevel.PUBLIC)
@AllArgsConstructor(access=AccessLevel.PUBLIC)
public class TemperatureReading {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_temperature_reading")
    private Long id;
    
    @Column(nullable=false)
    @NotNull(message="Zone ID is mandatory")
    private Long zoneId;
    
    @Column(nullable=false)
    @NotNull(message="Temperature in Celsius is mandatory")
    private BigDecimal temperatureCelsius;
    
    @Column(nullable=false)
    @NotNull(message="Reading time is mandatory")
    @Convert(converter = LocalDateTimeConverter.class)
    private LocalDateTime readingTime = LocalDateTime.now();

}
