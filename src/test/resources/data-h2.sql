insert into temperature_reading(id, zone_id, temperature_celsius, reading_time)
values (seq_temperature_reading.nextval, 1, 21.3, current_timestamp());

insert into temperature_reading(id, zone_id, temperature_celsius, reading_time)
values (seq_temperature_reading.nextval, 1, 22.1, current_timestamp());

insert into temperature_reading(id, zone_id, temperature_celsius, reading_time)
values (seq_temperature_reading.nextval, 2, 23.2, parsedatetime('13-05-2019 03:30:05.987', 'dd-MM-yyyy HH:mm:ss.SSS'));
