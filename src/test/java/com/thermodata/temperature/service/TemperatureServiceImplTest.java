package com.thermodata.temperature.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.thermodata.temperature.dto.TemperatureReadingDTO;
import com.thermodata.temperature.model.TemperatureReading;
import com.thermodata.temperature.repository.TemperatureRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TemperatureServiceImplTest {
    private TemperatureService temperatureService;

    @MockBean
    private TemperatureRepository temperatureRepository;

    @Autowired
    private ModelMapper modelMapper;
    
    private static final TemperatureReading TEMPERATURE_READING_TEMPLATE = new TemperatureReading(123L, null, new BigDecimal("23.4"), LocalDateTime.now());

    @Before
    public void setUp() {
        temperatureService = new TemperatureServiceImpl(temperatureRepository, modelMapper);
        
        Mockito.when(temperatureRepository.save(Mockito.any(TemperatureReading.class))).then(i -> i.getArgument(0));
        Mockito.when(temperatureRepository.findAllByZoneIdOrderByReadingTimeDesc(Mockito.anyLong())).then(this::buildListTemperatureReadingUsingZoneId);
    }
    
    private List<TemperatureReading> buildListTemperatureReadingUsingZoneId(InvocationOnMock i) {
        Long zoneId = i.getArgument(0);
        return Arrays.asList(new TemperatureReading(TEMPERATURE_READING_TEMPLATE.getId(), zoneId, TEMPERATURE_READING_TEMPLATE.getTemperatureCelsius(), TEMPERATURE_READING_TEMPLATE.getReadingTime()));
    }

    @Test
    public void whenSaveTemperatureReading_thenResponseShouldBeEqual() {
        final Long zoneId = 5L;
        TemperatureReadingDTO temperatureReadingDTO = new TemperatureReadingDTO(zoneId, TEMPERATURE_READING_TEMPLATE.getTemperatureCelsius(), TEMPERATURE_READING_TEMPLATE.getReadingTime());
        TemperatureReadingDTO savedTemperatureReading = temperatureService.saveTemperature(temperatureReadingDTO);
        
        assertThat(savedTemperatureReading).isNotNull();
        assertThat(savedTemperatureReading.getZoneId()).isEqualTo(temperatureReadingDTO.getZoneId());
        assertThat(savedTemperatureReading.getTemperatureCelsius()).isEqualTo(temperatureReadingDTO.getTemperatureCelsius());
        assertThat(savedTemperatureReading.getReadingTime()).isEqualTo(temperatureReadingDTO.getReadingTime());
    }

    @Test
    public void whenSaveTemperatureAndSearchItByZone_thenResponseShouldBeEqualToInput() {
        final Long zoneId = 7L;
        TemperatureReadingDTO temperatureReadingDTO = new TemperatureReadingDTO(zoneId, TEMPERATURE_READING_TEMPLATE.getTemperatureCelsius(), TEMPERATURE_READING_TEMPLATE.getReadingTime());
        temperatureService.saveTemperature(temperatureReadingDTO);

        List<TemperatureReadingDTO> foundTemperatureReadings = temperatureService.listTemperaturesByZoneId(zoneId);

        assertThat(foundTemperatureReadings).isNotEmpty();
        assertThat(foundTemperatureReadings.size()).isEqualTo(1L);
        assertThat(foundTemperatureReadings.get(0).getZoneId()).isEqualTo(temperatureReadingDTO.getZoneId());
        assertThat(foundTemperatureReadings.get(0).getTemperatureCelsius()).isEqualTo(temperatureReadingDTO.getTemperatureCelsius());
        assertThat(foundTemperatureReadings.get(0).getReadingTime()).isEqualTo(temperatureReadingDTO.getReadingTime());
    }

}