package com.thermodata.temperature.controller;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.thermodata.temperature.dto.TemperatureReadingDTO;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.config.JsonPathConfig;
import io.restassured.response.Response;
import static io.restassured.config.JsonConfig.jsonConfig;

// TODO create tests for more 400 responses and other errors once validations are implemented, such as temperatures range, timestamp, user access
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.DEFINED_PORT)
@TestPropertySource(properties = "server.port=8080")
public class TemperatureControllerTest {
    private static final String BASE_URL = "api/v1/temperature";
    private static final String URL_POST_SAVE_TEMPERATURE = BASE_URL;
    private static final String URL_GET_LIST_TEMPERATURE_BY_ZONEID = BASE_URL + "/{zoneId}";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
    
    @Before
    public void before() {
        RestAssured.config = RestAssured.config().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
    }

    @Test
    public void whenCreateTemperatureReading_thenResponseMatches() {
        final TemperatureReadingDTO temperatureReadingDTO = new TemperatureReadingDTO(3L, new BigDecimal("23.5"), LocalDateTime.now());
        
        Response responseCreateTemperature = getSaveTemperatureResponse(temperatureReadingDTO);
        responseCreateTemperature.prettyPrint();
        responseCreateTemperature
            .then()
            .assertThat()
            .statusCode(200)
            .body("zoneId", equalTo(temperatureReadingDTO.getZoneId().intValue()))
            .body("temperatureCelsius", equalTo(temperatureReadingDTO.getTemperatureCelsius()))
            .body("readingTime", equalTo(formatter.format(temperatureReadingDTO.getReadingTime())));
    }

    @Test
    public void whenCreateTemperatureReadingWithoutTemperatureCelsius_thenMessageIsThrown() {
        final TemperatureReadingDTO temperatureReadingDTO = new TemperatureReadingDTO(3L, null, LocalDateTime.now());
        
        Response responseCreateTemperature = getSaveTemperatureResponse(temperatureReadingDTO);
        responseCreateTemperature.prettyPrint();
        responseCreateTemperature
            .then()
            .assertThat()
            .statusCode(400)
            .body("message", equalTo("Temperature in Celsius is mandatory"));
    }

    @Test
    public void whenCreateTemperatureReadingWithoutZoneId_thenMessageIsThrown() {
        final TemperatureReadingDTO temperatureReadingDTO = new TemperatureReadingDTO(null, new BigDecimal("19.9"), LocalDateTime.now());
        
        Response responseCreateTemperature = getSaveTemperatureResponse(temperatureReadingDTO);
        responseCreateTemperature.prettyPrint();
        responseCreateTemperature
            .then()
            .assertThat()
            .statusCode(400)
            .body("message", equalTo("Zone ID is mandatory"));
    }

    // TODO this test should be reconsidered, since maybe when the timestamp is empty, we could do a LocalDate.now()
    // Or even not allow user to input timestamp
    @Test
    public void whenCreateTemperatureReadingWithoutReadingTime_thenMessageIsThrown() {
        final TemperatureReadingDTO temperatureReadingDTO = new TemperatureReadingDTO(3L, new BigDecimal("19.9"), null);
        
        Response responseCreateTemperature = getSaveTemperatureResponse(temperatureReadingDTO);
        responseCreateTemperature.prettyPrint();
        responseCreateTemperature
            .then()
            .assertThat()
            .statusCode(400)
            .body("message", equalTo("Reading time is mandatory"));
    }

    @Test
    public void whenListAlreadyExistingTemperatureByZoneId_thenResponseMatches() {
        final Long zoneId = 2L;
        
        Response responseListTemperatures = getListTemperatureByZoneIdResponse(zoneId);
        responseListTemperatures.prettyPrint();
        responseListTemperatures
            .then()
            .assertThat()
            .statusCode(200)
            .body("[0].zoneId", equalTo(2))
            .body("[0].temperatureCelsius", equalTo(new BigDecimal("23.20")))
            .body("[0].readingTime", equalTo("2019-05-13T03:30:05.987"));
    }

    @Test
    public void whenListTemperatureByZoneIdHasNoData_thenResponseIsEmpty() {
        final Long zoneIdWithNoData = 5L;
        
        Response responseListTemperaturesEmpty = getListTemperatureByZoneIdResponse(zoneIdWithNoData);
        responseListTemperaturesEmpty.prettyPrint();
        responseListTemperaturesEmpty
            .then()
            .assertThat()
            .statusCode(200)
            .body("", Matchers.emptyCollectionOf(TemperatureReadingDTO.class));
    }

    @Test
    public void whenCreateTemperatureReadingAndSearch_thenResponseMatches() {
        final Long zoneId = 99L;
        final TemperatureReadingDTO temperatureReadingDTO = new TemperatureReadingDTO(zoneId, new BigDecimal("21.90"), LocalDateTime.now());
        
        Response responseCreateTemperature = getSaveTemperatureResponse(temperatureReadingDTO);
        responseCreateTemperature.prettyPrint();
        
        Response responseListTemperature = getListTemperatureByZoneIdResponse(zoneId);
        responseListTemperature.prettyPrint();
        responseListTemperature
            .then()
            .assertThat()
            .statusCode(200)
            .body("[0].zoneId", equalTo(zoneId.intValue()))
            .body("[0].temperatureCelsius", equalTo(temperatureReadingDTO.getTemperatureCelsius()))
            .body("[0].readingTime", equalTo(formatter.format(temperatureReadingDTO.getReadingTime())));
    }

    private Response getSaveTemperatureResponse(final TemperatureReadingDTO temperatureReadingDTO) {
        return given()
            .contentType(ContentType.JSON)
            .body(temperatureReadingDTO)
            .when()
            .post(URL_POST_SAVE_TEMPERATURE);
    }

    private Response getListTemperatureByZoneIdResponse(final Long zoneId) {
        return given()
            .contentType(ContentType.JSON)
            .pathParam("zoneId", zoneId)
            .when()
            .get(URL_GET_LIST_TEMPERATURE_BY_ZONEID);
    }

}
