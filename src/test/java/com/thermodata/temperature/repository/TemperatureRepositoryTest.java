package com.thermodata.temperature.repository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;


import static org.assertj.core.api.Assertions.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.thermodata.temperature.model.TemperatureReading;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TemperatureRepositoryTest {

    @Autowired
    private TemperatureRepository temperatureRepository;

    @Test
    public void whenSaveTemperatureReadingAndFindByZoneId_thenReturnCorrespondingTemperatureReading() {
        final Long zoneId = 3L;
        
        // given
        TemperatureReading temperatureReading = new TemperatureReading(1L, zoneId, new BigDecimal("25.3"), LocalDateTime.now());
        temperatureRepository.save(temperatureReading);

        // when
        List<TemperatureReading> listTemperatureReading = temperatureRepository.findAllByZoneIdOrderByReadingTimeDesc(zoneId);

        // then
        assertThat(listTemperatureReading).isNotEmpty();
        assertThat(listTemperatureReading.size()).isEqualTo(1);
        assertThat(listTemperatureReading.get(0).getId()).isEqualTo(temperatureReading.getId());
        assertThat(listTemperatureReading.get(0).getZoneId()).isEqualTo(temperatureReading.getZoneId());
        assertThat(listTemperatureReading.get(0).getTemperatureCelsius()).isEqualTo(temperatureReading.getTemperatureCelsius());
        assertThat(listTemperatureReading.get(0).getReadingTime()).isEqualTo(temperatureReading.getReadingTime());
    }
    
}