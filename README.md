# Thermodata - Spring Boot

This is a simple temperature reading REST API made with Spring Boot with the following operations:

* save temperature reading
* list temperature reading by Zone ID

There is no authentication implemented and also no customer ID. That means that for this version, only one customer is expected.  
Below you can find some instructions on how to use it.

**Side note:** you may also access this application deployed on https://thermodata-springboot.herokuapp.com (deployed on September 2019). This might take a few seconds on the first time you access it.  

**LIMITATIONS**  
1. no access control (refer to desired features below on notes)  
2. no date or time validation (it accepts any date)  
3. no temperature validation (it will accept any decimal value)  

**NOTES**  
1. Desired Feature: access control like an endpoint with authentication that returns a JWT Token and use it throughout the application  
2. Desired Feature: to save and filter by customerId (embedded in JWT Token mentioned above) and have it working SaaS-like  
3. Desired Feature: another endpoint to filter by zoneId and date range, being both start and end optional filters  
4. Nothing was done with the "15 minutes" mentioned in the requirements, which could be a validation allowing only to save another reading after at least 15 mins from the previous  
5. Desired Feature: If user does not inform timestamp, it will get current timestamp. Or even maybe we should consider the possibility of user not inputting timestamp in the request. I've left it open in case of latency or lack of communication for a while 


## Start Thermodata Spring Boot (or use above already deployed springboot)
Requirements:  

* [git](https://git-scm.com/downloads)
* [maven](https://maven.apache.org/download.cgi)
* [Java 8+](https://www.java.com/download/) installed in your environment
* have mvn executable set in your PATH

Steps:

1. Clone this bitbucket repo in your local environment onto a folder of your choice executing below:  
 ```
 git clone https://paulobing@bitbucket.org/paulobing/thermodata.git
 ```
2. To start springboot execute from inside the downloaded directory with the project (it might take a few minutes the first time to download dependencies):  
 ```
 mvn spring-boot:run
 ```
3. Choose one of below endpoints or swagger-ui (instructions further below) to test the endpoints.

Tests:

There are 10 test cases and you may execute the tests or clean install to check full compile and tests execution (including rest-assured endpoint tests):  
 ```
 mvn clean install
 ```
 ```
 mvn test
 ```


## Swagger API Interface (note: remove quotes from 'T' from the timestamp model template)
To see the API in an interactive way, you can access below url (which will redirect to swagger-ui):  
```
http://localhost:8080/
```

# API Call Actions
As mentioned above, below is a list of endpoints available to perform each action with an example:

## Save Temperature Reading (POST)
In order to save a temperature reading, the endpoint is:
```java
POST localhost:8080/api/v1/temperature
```
Example:
```
curl -X POST "http://localhost:8080/api/v1/temperature" -H "Content-Type: application/json" -d '{ "zoneId": 1, "temperatureCelsius": 21.34, "readingTime": "2019-09-21T16:37:52.987"}'
```

## List Temperatures per Zone (GET)
To list temperature readings per Zone ID:
```java
POST localhost:8080/api/v1/temperature/{zoneId}
```
Example:
```
curl --request GET http://localhost:8080/api/v1/temperature/1
```
